#include <ESP8266WiFi.h>
#include <time.h>

const char * ssid = "Biofeeder Desarrollo";
const char * password = "biofeeder23";

#define getCompileTime() _getCompileTime(__DATE__, __TIME__)
struct tm * _getCompileTime(const char * compile_date, const char * compile_time){
  const char * months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

  int year = atoi(compile_date+7),
  month = 0,
  day = atoi(compile_date+4),
  hour = atoi(compile_time),
  minute = atoi(compile_time+3),
  second = atoi(compile_time+6);

  int i;
  for(i=0; i<12; i++){
    if(memcmp(compile_date, months[i], 3) == 0){
      month = i;
    }
  }

  static struct tm * timeinfo = (struct tm *)malloc(sizeof(struct tm));
  timeinfo->tm_year = year - 1900;
  timeinfo->tm_mon = month;
  timeinfo->tm_mday = day;
  timeinfo->tm_hour = hour;
  timeinfo->tm_min = minute;
  timeinfo->tm_sec = second;
  mktime(timeinfo);
  return timeinfo;
}

struct tm * getCurrentTime(){
  time_t rawtime;
  time (&rawtime);
  return localtime(&rawtime);
}

void printTime(struct tm * t){
  static char result[64];
  // see: http://www.cplusplus.com/reference/ctime/strftime/
  strftime (result,64,"%A, %B %d %Y %H:%M:%S",t);
  Serial.printf("Time: %s\n", result);
}

extern "C" void system_set_os_print(uint8 onoff);

bool initTime(){
  time_t rawtime;
  configTime(-5 * 3600, 0, "south-america.pool.ntp.org", "time.nist.gov");
  uint8_t i = 0;
  system_set_os_print(0);
  while(time(&rawtime) == 0 && i++ < 100) delay(10);
  system_set_os_print(1);
  if(i==100){
    Serial.println("Time Init Failed");
    return false;
  }
  printTime(getCurrentTime());
  return true;
}

void setup(){
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();
  Serial.print("Sketch Compile ");
  printTime(getCompileTime());
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    os_printf("WiFi Failed!\n");
    while(1){
      delay(100);
    }
  }
  initTime();
}

void loop(){
  delay(1000);
  printTime(getCurrentTime());
}